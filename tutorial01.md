# TypeScript Tutorial 01
## Hello World

### Step #1

1. Install **nodejs** **[npm](https://www.npmjs.com/)**. You can find the instructions for installation of npm 
[https://nodejs.org/en/download/](https://nodejs.org/en/download/). We need this to install TypeScript and many more things. 
2. Install **TypeScript** using command
        `npm install typescript`

### Step #2 
To get started we can open a text editor (I prefer emacs) and write our first program. 

```typescript
console.log("Hello World");
```

Save the file as `hello.js` in a folder TypeScript on your desktop. To run the program, open the command line terminal (cmd) and navigate to your Desktop directory using `cd` command e.g. if my user on windows is `Shazia', I would write

```
    C:\> node hello.js
```       


